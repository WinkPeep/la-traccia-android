package by.wink.latraccia.fragments;


import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import by.wink.latraccia.R;
import by.wink.latraccia.adapters.EventAdapter;
import by.wink.latraccia.controllers.EventController;

/**
 * Created by martin on 9/16/16.
 */
@EFragment(R.layout.fragment_events)
public class EVentsFragment extends Fragment {

  @ViewById(R.id.events_recycler)
  RecyclerView mRecyclerView;

  private EventController eventController;
  private EventAdapter eventAdapter;

  @AfterInject
  void doAfterInject(){
    eventController = new EventController();
    eventAdapter= new EventAdapter(eventController);

    eventController.registerDataListener(new EventController.EventListener() {
      @Override
      public void onDataSetChanged() {
        eventAdapter.notifyDataSetChanged();
      }
    });
    eventController.loadEvents();
  }

  @AfterViews
  void doAfterViews(){
    GridLayoutManager layoutManager = new GridLayoutManager(getActivity().getApplicationContext(), 2);
    mRecyclerView.setLayoutManager(layoutManager);
    mRecyclerView.setAdapter(eventAdapter);
    //mNewsFeedRecycler.addItemDecoration(new NewsItemDivider(getContext()));
  }

  @Override
  public void onDestroy() {
    eventController.unregisterDataListener();
    super.onDestroy();
  }
}
