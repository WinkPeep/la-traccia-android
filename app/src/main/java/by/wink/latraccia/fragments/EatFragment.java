package by.wink.latraccia.fragments;

import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.ViewsById;

import java.util.ArrayList;

import by.wink.latraccia.R;
import by.wink.latraccia.adapters.EatAdapter;
import by.wink.latraccia.controllers.EatController;

/**
 * Created by martin on 9/19/16.
 */
@EFragment(R.layout.fragment_eat)
public class EatFragment extends Fragment {

    @ViewById(R.id.eat_recycler)
    RecyclerView eat_fragment;

    private EatController eatController;
    private EatAdapter eatAdapter;


    @AfterInject
    void doAfterInject() {
        eatController = new EatController();
        eatAdapter = new EatAdapter(getContext(),eatController);

        eatController.registerDataListener(new EatController.EatListener() {
            @Override
            public void onDataSetChanged() {

                eatAdapter.notifyDataSetChanged();
            }
        });
        eatController.loadEat();
    }

    @AfterViews
    void doAfterViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        eat_fragment.setLayoutManager(layoutManager);
        eat_fragment.setAdapter(eatAdapter);

    }

    @Override
    public void onDestroy() {
        eatController.unregisterDataListener();
        super.onDestroy();
    }
}
