package by.wink.latraccia.fragments;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import by.wink.latraccia.R;
import by.wink.latraccia.adapters.EatAdapter;
import by.wink.latraccia.adapters.ListEatAdapter;
import by.wink.latraccia.controllers.ListEatController;

@EFragment(R.layout.fragment_list_item_eat)
public class ListItemEatFragment extends Fragment {
    private static View view;

    @ViewById(R.id.list_eat_recycler)
    RecyclerView list_eat_fragment;

    private ListEatController listEatController;
    private ListEatAdapter listEatAdapter;



    @AfterInject
    void doAfterInject() {
        listEatController = new ListEatController();
        listEatAdapter = new ListEatAdapter(listEatController);
        listEatController.registerDataListener(new ListEatController.ListEatListener() {
            @Override
            public void onDataSetChanged() {
                listEatAdapter.notifyDataSetChanged();
            }
        });

        listEatController.loadListEat();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);

        }
        try {
            view = inflater.inflate(R.layout.item_list_eat, container, false);
        } catch (InflateException e) {
    /* map is already there, just return view as it is */
        }
        return view;
    }

    @AfterViews
    void doAfterViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        list_eat_fragment.setLayoutManager(layoutManager);
        list_eat_fragment.setAdapter(listEatAdapter);

    }

    @Override
    public void onDestroy() {
        listEatController.unregisterDataListener();
        super.onDestroy();
    }
}