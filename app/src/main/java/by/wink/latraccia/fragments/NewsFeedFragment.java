package by.wink.latraccia.fragments;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import by.wink.latraccia.R;
import by.wink.latraccia.adapters.NewsFeedRecyclerAdapter;
import by.wink.latraccia.controllers.NewsController;

/**
 * Created by martin on 8/26/16.
 */
@EFragment(R.layout.fragment_newsfeed)
public class NewsFeedFragment extends Fragment {

  private final static String LOG_TAG = NewsFeedFragment.class.getSimpleName();

  @ViewById(R.id.newsfeed_recycler)
  RecyclerView mNewsFeedRecycler;

  private NewsController newsController;
  private NewsFeedRecyclerAdapter mNewsAdapter;

  @AfterInject
  void doAfterInject(){
    newsController = new NewsController();
    mNewsAdapter = new NewsFeedRecyclerAdapter(newsController);
    newsController.registerDataListener(new NewsController.NewsFeedListener() {
      @Override
      public void onDataSetChanged() {
        mNewsAdapter.notifyDataSetChanged();
      }
    });
    newsController.loadNews();
  }

  @AfterViews
  void doAfterViews(){
    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
    mNewsFeedRecycler.setLayoutManager(layoutManager);
    mNewsFeedRecycler.setAdapter(mNewsAdapter);
    //mNewsFeedRecycler.addItemDecoration(new NewsItemDivider(getContext()));
  }

  @Override
  public void onDestroy() {
    newsController.unregisterDataListener();
    super.onDestroy();
  }
}
