package by.wink.latraccia.utils;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by martin on 9/16/16.
 */
public class StorageWrapper {

  private  static  FirebaseStorage storage;
  private static StorageReference storageRef;
  private  static StorageReference imagesRef;

  public static void init(){
    storage = FirebaseStorage.getInstance();
    storageRef = storage.getReferenceFromUrl("gs://latraccia-eb7a7.appspot.com");
    imagesRef = storageRef.child("images");
  }

  public static StorageReference getPicRef(String media){
    return imagesRef.child(media) ;
  }
}
