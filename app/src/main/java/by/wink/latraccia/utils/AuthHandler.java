package by.wink.latraccia.utils;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import org.androidannotations.annotations.EBean;

/**
 * Created by martin on 9/22/16.
 */
@EBean(scope = EBean.Scope.Singleton)
public class AuthHandler {

  public interface LtStateListener{
    void onLoggedIn();
    void onLoggedOut();
  }

  private LtStateListener ltStateListener = null;

  public void register(@NonNull FirebaseAuth.AuthStateListener listener){
    FirebaseAuth.getInstance().addAuthStateListener(listener);
  }
  public void unregister(@NonNull FirebaseAuth.AuthStateListener listener){
    FirebaseAuth.getInstance().removeAuthStateListener(listener);
  }

  /*
  public synchronized void register(){
      mAuthListener = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
          FirebaseUser user = firebaseAuth.getCurrentUser();
          if (user != null) {
            if ( ltStateListener != null ){
              ltStateListener.onLoggedIn();
            }
          } else {
            if(ltStateListener != null ){
              ltStateListener.onLoggedOut();
            }
          }
        }
      };
      FirebaseAuth.getInstance().addAuthStateListener(mAuthListener);
  }

  public synchronized void unregister(){
    if( mAuthListener != null ){
      FirebaseAuth.getInstance().removeAuthStateListener(mAuthListener);
      mAuthListener = null;
      mInit = false;
    }
  }

  public void addAuthListener(LtStateListener ltStateListener){
    this.ltStateListener = ltStateListener;
  }

  public void removeAuthListener(){
    this.ltStateListener = null;
  }
  */

  public boolean isLoggedIn(){
    return (FirebaseAuth.getInstance().getCurrentUser() != null );
  }

  public boolean isAnonymous(){
    if ( isLoggedIn() ){
      return FirebaseAuth.getInstance().getCurrentUser().isAnonymous();
    }
    return false;
  }
  public String getUserDisplayName(){
    return FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
  }

  public void loginWithEmail(String email, String password){

    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
      @Override
      public void onComplete(@NonNull Task<AuthResult> task) {
        if (task.isSuccessful()){
          Log.d("BLABLA","completed ok");
        }else{
          Log.d("BLABLA","error loggini n");
        }
      }
    });

  }

  public interface OnAnonymCompleteListener{
    void onComplete(boolean result);
  }

  public void loginAnonymously(@NonNull final OnAnonymCompleteListener listener){
    FirebaseAuth.getInstance().signInAnonymously().addOnCompleteListener(
      new OnCompleteListener<AuthResult>() {
        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
          listener.onComplete(task.isSuccessful());
        }
      }
    );
  }

  public void logout(){
    FirebaseAuth.getInstance().signOut();
  }
}
