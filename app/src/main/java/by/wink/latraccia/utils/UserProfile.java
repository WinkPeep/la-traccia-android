package by.wink.latraccia.utils;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.CompoundButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.octicons_typeface_library.Octicons;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import by.wink.latraccia.R;
import by.wink.latraccia.activities.UserProfileActivity;

/**
 * Created by martin on 9/26/16.
 */

@EBean(scope = EBean.Scope.Singleton)
public class UserProfile {
  private static final int OPTION_MY_PROFILE = 1000;
  private static final int OPTION_MY_CALENDAR = 2000;
  private static final int OPTION_MY_EVENTS = 3000;
  private static final int OPTION_SETTINGS = 4000;
  private static final int OPTION_NOTIFICATIONS = 5000;
  private static final int OPTION_LOGOUT = 6000;
  private static final int OPTION_LOGIN = 7000;
  private static final int OPTION_CREATE_ACCOUNT = 8000;

  private IProfile mUserDrawerProfile;

  @Bean
  AuthHandler authHandler;

  public @NonNull IProfile getUserDrawerProfile(){

    if ( mUserDrawerProfile != null ){
      return mUserDrawerProfile;
    }

    if ( authHandler.isLoggedIn() ) {
      if (authHandler.isAnonymous()) {
        mUserDrawerProfile = new ProfileDrawerItem().withName("Anonymous guest").
          withIcon("https://avatars3.githubusercontent.com/u/1476232?v=3&s=460");
      }else {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String displayName  = user.getDisplayName();
        if ( displayName == null ){
          displayName = "< you name >";
        }
        String email = user.getEmail();

        mUserDrawerProfile = new ProfileDrawerItem().withName(displayName).
          withEmail(email).
          withIcon("https://avatars3.githubusercontent.com/u/1476232?v=3&s=460").withIdentifier(100);
      }
    }else{
      mUserDrawerProfile = new ProfileDrawerItem().withName("Not logged in");
    }

    return mUserDrawerProfile;
  }

  public @NonNull IDrawerItem[] getUserDrawerOptions(){

    List<IDrawerItem> options = new ArrayList<>();

    if ( authHandler.isLoggedIn() ){
      if ( authHandler.isAnonymous() ){
        options.add(new PrimaryDrawerItem()
          .withName(R.string.option_login)
          .withIcon(Octicons.Icon.oct_log_in)
          .withIdentifier(OPTION_LOGIN)
          .withSelectable(false));

        options.add(new PrimaryDrawerItem()
          .withName(R.string.option_create_account)
          .withIcon(Octicons.Icon.oct_person_add)
          .withIdentifier(OPTION_CREATE_ACCOUNT)
          .withSelectable(false));
      }else{
        options.add(new PrimaryDrawerItem()
          .withName(R.string.options_my_profile)
          .withIcon(Octicons.Icon.oct_person)
          .withIdentifier(OPTION_MY_PROFILE)
          .withSelectable(false));

        options.add(new PrimaryDrawerItem()
          .withName(R.string.option_my_events)
          .withIcon(Octicons.Icon.oct_browser)
          .withIdentifier(OPTION_MY_EVENTS)
          .withSelectable(false));

        options.add(new PrimaryDrawerItem()
          .withName(R.string.option_my_calendar)
          .withIcon(Octicons.Icon.oct_calendar)
          .withIdentifier(OPTION_MY_CALENDAR)
          .withSelectable(false));

        options.add(new DividerDrawerItem());

        options.add(new PrimaryDrawerItem()
          .withName(R.string.option_settings)
          .withIcon(Octicons.Icon.oct_settings)
          .withIdentifier(OPTION_SETTINGS)
          .withSelectable(false));

        options.add(new SwitchDrawerItem()
          .withName(R.string.option_notifications)
          .withIcon(Octicons.Icon.oct_tools)
          .withIdentifier(OPTION_NOTIFICATIONS)
          .withChecked(true)
          .withOnCheckedChangeListener(onCheckedChangeListener));
      }

      options.add(new DividerDrawerItem());

      options.add(new PrimaryDrawerItem()
        .withName(R.string.option_logout)
        .withIcon(Octicons.Icon.oct_log_out)
        .withIdentifier(OPTION_LOGOUT)
        .withSelectable(false));
    }

    return options.toArray(new IDrawerItem[0]);
  }

  public boolean handleOptionClick(Context c,long id) {
    switch ((int)id) {
      case OPTION_MY_PROFILE:
        Intent intent = new Intent(c, UserProfileActivity.class);
        c.startActivity(intent);
        return true;
      case OPTION_LOGOUT:
        authHandler.logout();
        return true;
      default:
        return false;
    }
  }

  private OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(IDrawerItem drawerItem, CompoundButton buttonView, boolean isChecked) {
      if (drawerItem instanceof Nameable) {
        Log.i("material-drawer", "DrawerItem: " + ((Nameable) drawerItem).getName() + " - toggleChecked: " + isChecked);
      } else {
        Log.i("material-drawer", "toggleChecked: " + isChecked);
      }
    }
  };

}
