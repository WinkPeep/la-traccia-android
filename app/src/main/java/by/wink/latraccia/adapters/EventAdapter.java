package by.wink.latraccia.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;

import java.io.File;
import java.lang.ref.WeakReference;

import by.wink.latraccia.R;
import by.wink.latraccia.activities.EvensActivity;
import by.wink.latraccia.controllers.EventController;
import by.wink.latraccia.models.EventItem;
import by.wink.latraccia.utils.StorageWrapper;

/**
 * Created by martin on 9/16/16.
 */
public class EventAdapter extends RecyclerView.Adapter {
  private WeakReference<EventController> eventController;

  public EventAdapter(EventController controller){
    this.eventController = new WeakReference<>(controller);
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new EventHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_card,parent, false));
  }

  @Override
  public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
    if ( holder instanceof EventHolder){
      if ( eventController.get() != null ) {
        final EventItem item = eventController.get().getItemAt(position);
        ((EventHolder) holder).bind(holder.itemView.getContext(), item);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), EvensActivity.class);
            intent.putExtra("media_name", item.media);

            holder.itemView.getContext().startActivity(intent);

          }
        });
      }
    }

  }

  @Override
  public int getItemCount() {
    if ( eventController.get() == null ) return 0;

    return eventController.get().getDataCount();
  }

  private class EventHolder extends RecyclerView.ViewHolder{
    public TextView titleText;
    public ImageView eventPic;
    //public TextView descText;

    public EventHolder(View itemView) {
      super(itemView);
      titleText = (TextView) itemView.findViewById(R.id.event_card_title);
      eventPic = (ImageView) itemView.findViewById(R.id.event_card_pic);
      //descText = (TextView) itemView.findViewById(R.id.item_news_desc);
    }

    public void bind(final Context context, EventItem item){
      titleText.setText(item.title);
      //Glide.with(context).load(R.drawable.welcome).into(eventPic);

      try {
        final File localFile = File.createTempFile(item.media, "jpg");

        StorageWrapper.getPicRef(item.media).getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
          @Override
          public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
            // Local temp file has been created
            Glide.with(context).load(localFile).into(eventPic);
          }
        }).addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception exception) {
            // Handle any errors
          }
        });
      }catch (Exception e){
        Log.d("BLABLA","failed to create local file");

      }
    }
  }
}
