package by.wink.latraccia.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;

import java.io.File;
import java.lang.ref.WeakReference;

import by.wink.latraccia.R;
import by.wink.latraccia.activities.EvensActivity;
import by.wink.latraccia.activities.ListEatActivity;
import by.wink.latraccia.controllers.EatController;
import by.wink.latraccia.fragments.ListItemEatFragment;
import by.wink.latraccia.models.EatItem;
import by.wink.latraccia.utils.StorageWrapper;

/**
 * Created by martin on 10/3/16.
 */
public class EatAdapter extends RecyclerView.Adapter {
  private WeakReference<EatController> eatController;
  Context context;

  public EatAdapter(Context c,EatController controller){
    this.eatController = new WeakReference<>(controller);
   eatController = new WeakReference<>(controller);
    this.context = c;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new EatHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_eat,parent,false));
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    if ( holder instanceof EatHolder){
      if (eatController.get() != null);
      EatItem item = eatController.get().getItemAt(position);
      ((EatHolder) holder).titleText.setText(item.name);
      ((EatHolder) holder).bind(holder.itemView.getContext(), item);

    }

  }

  @Override
  public int getItemCount() {
    if (eatController.get() == null) return 0;
    return eatController.get().getDataCount();
  }
  private class EatHolder extends RecyclerView.ViewHolder {

    public TextView titleText;
    public ImageView eatPic;
    Context c;
    //public TextView descText;

    public EatHolder(View  itemView) {
      super(itemView);
      titleText = (TextView) itemView.findViewById(R.id.eat_card_title);
      eatPic = (ImageView) itemView.findViewById(R.id.eat_img);
      itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        //  ListItemEatFragment listItemEatFragment = new ListItemEatFragment();
        //  ((ListEatActivity) context).getSupportFragmentManager().beginTransaction().replace(R.id.eat_recycler,listItemEatFragment).commit();
          context.startActivity(new Intent(context, ListEatActivity.class));
        }
      });
    }
    public void bind(final Context context, EatItem item) {
      //Glide.with(context).load(R.drawable.welcome).into(eventPic);

      if (eatPic != null) {
        try {
          final File localFile = File.createTempFile(item.pic, "jpg");
          StorageWrapper.getPicRef(item.pic).getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
              Glide.with(context).load(localFile).into(eatPic);
            }
          }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
          });
        } catch (Exception e) {
          Log.d("hellow", "hellow");
        }

      }
    }
  }
}
