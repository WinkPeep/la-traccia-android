package by.wink.latraccia.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import by.wink.latraccia.R;
import by.wink.latraccia.controllers.NewsController;
import by.wink.latraccia.models.NewsItem;

/**
 * Created by martin on 8/26/16.
 */
public class NewsFeedRecyclerAdapter extends RecyclerView.Adapter {

  //private NewsController newsController;
  private WeakReference<NewsController> newsController;

  public NewsFeedRecyclerAdapter(NewsController controller){
    newsController = new WeakReference<NewsController>(controller);
  }


  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new NewsHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_card,parent, false));
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
    if ( holder instanceof NewsHolder ){
      NewsItem item = newsController.get().getItemAt(position);
      ( (NewsHolder) holder).titleText.setText(item.title);
      ( (NewsHolder) holder).descText.setText(item.desc);
    }

  }

  @Override
  public int getItemCount() {
    return newsController.get().getDataCount();
  }

  private class NewsHolder extends RecyclerView.ViewHolder{
    public TextView titleText;
    public TextView descText;

    public NewsHolder(View itemView) {
      super(itemView);
      titleText = (TextView) itemView.findViewById(R.id.item_news_title);
      descText = (TextView) itemView.findViewById(R.id.item_news_desc);
    }
  }
}
