package by.wink.latraccia.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;

import java.io.File;
import java.lang.ref.WeakReference;

import by.wink.latraccia.R;
import by.wink.latraccia.controllers.ListEatController;
import by.wink.latraccia.models.ListEatItem;
import by.wink.latraccia.utils.StorageWrapper;

/**
 * Created by wink on 05/10/16.
 */
public class ListEatAdapter extends RecyclerView.Adapter {
    private WeakReference<ListEatController> listEatController;

    public ListEatAdapter(ListEatController controller){
        this.listEatController = new WeakReference<>(controller);
        listEatController = new WeakReference<>(controller);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ListEatHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_eat,parent,false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if ( holder instanceof ListEatHolder){
            if (listEatController.get() != null);
            ListEatItem item = listEatController.get().getItemAt(position);
            ((ListEatHolder) holder).titleText.setText(item.name);
            ((ListEatHolder) holder).bind(holder.itemView.getContext(), item);

        }

    }

    @Override
    public int getItemCount() {
        //if (listEatController.get() == null) return 0;
      //  return listEatController.get().getDataCount();
        return 5;
    }
    private class ListEatHolder extends RecyclerView.ViewHolder {

        public TextView titleText;
        public ImageView eatPic;
        //public TextView descText;

        public ListEatHolder(View itemView) {
            super(itemView);
            titleText = (TextView) itemView.findViewById(R.id.list_eat_card_title);
            eatPic = (ImageView) itemView.findViewById(R.id.list_eat_img);

        }
        public void bind(final Context context, ListEatItem item) {
            //Glide.with(context).load(R.drawable.welcome).into(eventPic);

            if (eatPic != null) {
                try {
                    final File localFile = File.createTempFile(item.pic, "jpg");
                    StorageWrapper.getPicRef(item.pic).getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            Glide.with(context).load(localFile).into(eatPic);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {

                        }
                    });
                } catch (Exception e) {
                    Log.d("hellow", "hellow");
                }

            }
        }
    }
}
