package by.wink.latraccia.activities;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EActivity;

import by.wink.latraccia.R;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
@EActivity
public class WelcomeActivity extends AppCompatActivity {
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    Window window = getWindow();
    window.setFormat(PixelFormat.RGBA_8888);
  }

  Thread splashTread;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);
    StartAnimations();
    getSupportActionBar().hide();
    TextView t = (TextView)findViewById(R.id.textView1);
    t.setText("lascia la tua traccia");
    Animation animationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fadein);
    t.startAnimation(animationFadeIn);

  }

  private void StartAnimations() {
    Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
    anim.reset();
    LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);

    l.clearAnimation();
    l.startAnimation(anim);


    anim = AnimationUtils.loadAnimation(this, R.anim.translate);
    anim.reset();
    ImageView iv = (ImageView) findViewById(R.id.splash);
    iv.clearAnimation();

    iv.startAnimation(anim);

    splashTread = new Thread() {
      @Override
      public void run() {
        try {
          int waited = 0;
          // Splash screen pause time
          while (waited < 3000) {
            sleep(100);
            waited += 100;
          }
          Intent intent = new Intent(WelcomeActivity.this,
            IntroActivity.class);
          intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
          startActivity(intent);
          WelcomeActivity.this.finish();
        } catch (InterruptedException e) {
          // do nothing
        } finally {
          WelcomeActivity.this.finish();
        }

      }
    };
    splashTread.start();

  }

}
