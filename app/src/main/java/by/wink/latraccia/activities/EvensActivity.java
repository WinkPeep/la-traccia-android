package by.wink.latraccia.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;

import java.io.File;

import by.wink.latraccia.R;
import by.wink.latraccia.adapters.EventAdapter;
import by.wink.latraccia.utils.StorageWrapper;

public class EvensActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evens);


        Intent intent = getIntent();

        intent.getExtras().getString("i");

        String m = intent.getExtras().getString("media_name");

        try {
            final File localFile = File.createTempFile(m, "jpg");

            StorageWrapper.getPicRef(m).getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    // Local temp file has been created
                    // replace eventPic con imageview instance
                    ImageView eventPic = (ImageView)findViewById(R.id.imageView1);
                    Glide.with(getApplicationContext()).load(localFile).into(eventPic);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                }
            });
        }catch (Exception e){
            Log.d("BLABLA","failed to create local file");

        }



    }
}
