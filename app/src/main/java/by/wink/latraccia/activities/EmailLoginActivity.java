package by.wink.latraccia.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import by.wink.latraccia.MainActivity_;
import by.wink.latraccia.R;
import by.wink.latraccia.utils.AuthHandler;

@EActivity(R.layout.activity_email_login)
public class EmailLoginActivity extends AppCompatActivity {

 @Bean
 AuthHandler authHandler;

 @ViewById
 AutoCompleteTextView email;

 @ViewById
 EditText password;

 private FirebaseAuth.AuthStateListener authStateListener;

 @Click
 void email_sign_in_button(){
  String emailTxt = email.getText().toString();
  String pass = password.getText().toString();

  Log.d("BLABLA", "e: " + emailTxt);

  authHandler.loginWithEmail(emailTxt, pass);

 }

 @Override
 protected void onStart() {
  super.onStart();
  authStateListener = new FirebaseAuth.AuthStateListener() {
   @Override
   public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
    if ( firebaseAuth.getCurrentUser() != null ){
     Intent intent = new Intent(getApplicationContext(), MainActivity_.class);
     startActivity(intent);
     finish();
    }
   }
  };

  authHandler.register(authStateListener);
 }

 @Override
 protected void onStop() {
  authHandler.unregister(authStateListener);
  super.onStop();
 }
}

