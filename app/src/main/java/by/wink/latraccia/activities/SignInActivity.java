package by.wink.latraccia.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import by.wink.latraccia.R;
import by.wink.latraccia.utils.AuthHandler;

@EActivity(R.layout.activity_sign_in)
public class SignInActivity extends AppCompatActivity {

  @Bean
  AuthHandler authHandler;

  private FirebaseAuth.AuthStateListener authStateListener;

  @ViewById
  EditText edit_email;

  @ViewById
  EditText input_password;

  @Click
  void btn_login(){
    String emailTxt = edit_email.getText().toString();
    String pass = input_password.getText().toString();
    authHandler.loginWithEmail(emailTxt, pass);
  }

  @Override
  protected void onStart() {
    super.onStart();
    authStateListener = new FirebaseAuth.AuthStateListener() {
      @Override
      public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        if ( firebaseAuth.getCurrentUser() != null ){
          Intent intent = new Intent(getApplicationContext(), by.wink.latraccia.MainActivity.class);
          startActivity(intent);
          finish();
        }
      }
    };

    authHandler.register(authStateListener);
  }

  @Override
  protected void onStop() {
    authHandler.unregister(authStateListener);
    super.onStop();
  }
/*
  @Click
  void link_signup(){
    Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
    //startActivityForResult(intent, REQUEST_SIGNUP);
  }
  */

  @Override
  public void onBackPressed() {
    // disable going back to the MainActivity
    //TODO: invstigate this call
    moveTaskToBack(true);
  }
}
