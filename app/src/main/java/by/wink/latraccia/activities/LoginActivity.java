package by.wink.latraccia.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;

import by.wink.latraccia.MainActivity_;
import by.wink.latraccia.R;
import by.wink.latraccia.utils.AuthHandler;

@EActivity(R.layout.activity_login)
public class LoginActivity extends AppCompatActivity {

  @Bean
  AuthHandler authHandler;


  @Click
  void btn_log(){
    Intent intentlog = new Intent(LoginActivity.this, SignInActivity_.class);
    startActivity(intentlog);
  }


    @Click
  void login_guest(){
    authHandler.loginAnonymously(new AuthHandler.OnAnonymCompleteListener() {
      @Override
      public void onComplete(boolean result) {
        Intent intent = new Intent(getApplicationContext(), MainActivity_.class);
        startActivity(intent);
      }
    });
  }

  @Click
  void sign_up(){
    Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
    startActivity(intent);

  }

  @Override
  public void onStart() {
    super.onStart();
    authHandler.register( new FirebaseAuth.AuthStateListener() {
      @Override
      public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
          Intent intent = new Intent(getApplicationContext(), MainActivity_.class);
          startActivity(intent);
        }else{
          //DISPLAY ERROR !?!
          Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT);
        }
      }
    });
  }


}
