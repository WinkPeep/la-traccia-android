package by.wink.latraccia;

import android.app.Application;

import com.squareup.leakcanary.LeakCanary;

/**
 * Created by martin on 9/19/16.
 */
public class MainAppLauncher extends Application {

  @Override
  public void onCreate() {
    super.onCreate();
    if (LeakCanary.isInAnalyzerProcess(this)) {
      // This process is dedicated to LeakCanary for heap analysis.
      // You should not init your app in this process.
      return;
    }
    LeakCanary.install(this);
  }
}
