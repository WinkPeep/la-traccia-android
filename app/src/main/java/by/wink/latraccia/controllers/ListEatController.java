package by.wink.latraccia.controllers;

import org.androidannotations.annotations.EBean;


import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import by.wink.latraccia.models.EatItem;
import by.wink.latraccia.models.ListEatItem;

/**
 * Created by martin on 10/3/16.
 */
@EBean
public class ListEatController {
    private FirebaseDatabase mFbDatabase;
    private DatabaseReference mNewsReference;

    private HashMap<String, Integer> mListEatData = new HashMap<>();
    private List<ListEatItem> mEatList = new ArrayList<>();

    private ListEatListener mDataListener;

    public interface ListEatListener {
        void onDataSetChanged();
    }

    public int getDataCount() {
        return mEatList.size();
    }

    public ListEatItem getItemAt(int position) {
        return mEatList.get(position);
    }

    public ListEatController() {
        mFbDatabase = FirebaseDatabase.getInstance();
        mNewsReference = mFbDatabase.getReference("listeat");
    }

    public void registerDataListener(ListEatListener dataListener) {
        this.mDataListener = dataListener;
    }

    public void unregisterDataListener() {
        this.mDataListener = null;
    }

    public void loadListEat() {
        mNewsReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                ListEatItem item = dataSnapshot.getValue(ListEatItem.class);

                mEatList.add(0, item);

                mListEatData.put(dataSnapshot.getKey(), mEatList.size());

                if (mDataListener != null) {
                    mDataListener.onDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
