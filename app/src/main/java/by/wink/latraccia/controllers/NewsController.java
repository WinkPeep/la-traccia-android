package by.wink.latraccia.controllers;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import by.wink.latraccia.models.NewsItem;

/**
 * Created by martin on 8/26/16.
 */
public class NewsController {
  private FirebaseDatabase mFbDatabase;
  private DatabaseReference mNewsReference;

  private HashMap<String,Integer> mNewsData = new HashMap<>();
  private List<NewsItem> mNewsList = new ArrayList<>();

  private NewsFeedListener mDataListener;

  public interface NewsFeedListener{
    void onDataSetChanged();
  }

  public int getDataCount(){
    return mNewsList.size();
  }

  public NewsItem getItemAt(int position){
    return mNewsList.get(position);
  }

  public NewsController() {
    mFbDatabase = FirebaseDatabase.getInstance();
    mNewsReference = mFbDatabase.getReference("news");
  }

  public void registerDataListener(NewsFeedListener dataListener){
    this.mDataListener = dataListener;
  }

  public void unregisterDataListener(){
    this.mDataListener = null;
  }

  public void loadNews(){
    mNewsReference.addChildEventListener(new ChildEventListener() {
      @Override
      public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        NewsItem item = dataSnapshot.getValue(NewsItem.class);

        mNewsList.add(0,item);

        mNewsData.put(dataSnapshot.getKey(), mNewsList.size());

        if ( mDataListener != null ) {
          mDataListener.onDataSetChanged();
        }
      }

      @Override
      public void onChildChanged(DataSnapshot dataSnapshot, String s) {

      }

      @Override
      public void onChildRemoved(DataSnapshot dataSnapshot) {

      }

      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {

      }

      @Override
      public void onCancelled(DatabaseError databaseError) {

      }
    });
  }
}
