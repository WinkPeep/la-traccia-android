package by.wink.latraccia.controllers;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import by.wink.latraccia.models.EatItem;

/**
 * Created by martin on 10/3/16.
 */
@EBean
public class EatController {
  private FirebaseDatabase mFbDatabase;
  private DatabaseReference mNewsReference;

  private HashMap<String,Integer> mEatData = new HashMap<>();
  private List<EatItem> mEatList= new ArrayList<>();

  private EatListener mDataListener;

  public interface EatListener{
    void onDataSetChanged();
  }

  public int getDataCount(){
    return mEatList.size();
  }

  public EatItem getItemAt(int position){
    return mEatList.get(position);
  }

  public EatController() {
    mFbDatabase = FirebaseDatabase.getInstance();
    mNewsReference = mFbDatabase.getReference("eat");
  }

  public void registerDataListener(EatListener dataListener){
    this.mDataListener = dataListener;
  }

  public void unregisterDataListener(){
    this.mDataListener = null;
  }

  public void loadEat(){
    mNewsReference.addChildEventListener(new ChildEventListener() {
      @Override
      public void onChildAdded(DataSnapshot dataSnapshot, String s) {

        EatItem item = dataSnapshot.getValue(EatItem.class);

        mEatList.add(0,item);

        mEatData.put(dataSnapshot.getKey(), mEatList.size());

        if ( mDataListener !=null) {
          mDataListener.onDataSetChanged();
        }
      }

      @Override
      public void onChildChanged(DataSnapshot dataSnapshot, String s) {

      }

      @Override
      public void onChildRemoved(DataSnapshot dataSnapshot) {

      }

      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {

      }

      @Override
      public void onCancelled(DatabaseError databaseError) {

      }
    });
  }
}
