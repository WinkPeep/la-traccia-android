package by.wink.latraccia.controllers;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import by.wink.latraccia.models.EventItem;

/**
 * Created by martin on 9/16/16.
 */
public class EventController {
  private FirebaseDatabase mFbDatabase;
  private DatabaseReference mNewsReference;

  private HashMap<String,Integer> mEventData = new HashMap<>();
  private List<EventItem> mEVentList= new ArrayList<>();

  private EventListener mDataListener;

  public interface EventListener{
    void onDataSetChanged();
  }

  public int getDataCount(){
    return mEVentList.size();
  }

  public EventItem getItemAt(int position){
    return mEVentList.get(position);
  }

  public EventController() {
    mFbDatabase = FirebaseDatabase.getInstance();
    mNewsReference = mFbDatabase.getReference("events");
  }

  public void registerDataListener(EventListener dataListener){
    this.mDataListener = dataListener;
  }

  public void unregisterDataListener(){
    this.mDataListener = null;
  }

  public void loadEvents(){
    mNewsReference.addChildEventListener(new ChildEventListener() {
      @Override
      public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Log.d("BLABLA","Event got " + dataSnapshot.getKey());
        EventItem item = dataSnapshot.getValue(EventItem.class);
        Log.d("BLABLA","Event got title " +  item.title);


        mEVentList.add(0,item);

        mEventData.put(dataSnapshot.getKey(), mEVentList.size());

        if ( mDataListener !=null) {
          mDataListener.onDataSetChanged();
        }
      }

      @Override
      public void onChildChanged(DataSnapshot dataSnapshot, String s) {

      }

      @Override
      public void onChildRemoved(DataSnapshot dataSnapshot) {

      }

      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {

      }

      @Override
      public void onCancelled(DatabaseError databaseError) {

      }
    });
  }
}
