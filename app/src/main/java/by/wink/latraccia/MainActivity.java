package by.wink.latraccia;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mikepenz.itemanimators.AlphaCrossFadeAnimator;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import by.wink.latraccia.activities.LoginActivity_;
import by.wink.latraccia.fragments.EVentsFragment_;
import by.wink.latraccia.fragments.EatFragment_;
import by.wink.latraccia.fragments.NewsFeedFragment_;
import by.wink.latraccia.utils.AuthHandler;
import by.wink.latraccia.utils.StorageWrapper;
import by.wink.latraccia.utils.UserProfile;

/**
 * Created by martin on 8/25/16.
 */
@EActivity
public class MainActivity extends AppCompatActivity{

  @ViewById
  Toolbar toolbar;

  @ViewById
  ViewPager viewpager;

  @ViewById
  TabLayout tabs;

  @Bean
  UserProfile mUserProfile;

  @Bean
  AuthHandler authHandler;

  private FirebaseAuth.AuthStateListener mAuthListener;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    setSupportActionBar(toolbar);

    overridePendingTransition(R.animator.slide_left_in, R.animator.slide_left_out);

    setupViewPager(viewpager);
    tabs.setupWithViewPager(viewpager);

    AccountHeader headerResult = new AccountHeaderBuilder()
      .withActivity(this)
      .withTranslucentStatusBar(true)
      .withHeaderBackground(R.drawable.header)
      .addProfiles(
        mUserProfile.getUserDrawerProfile()
      )
      .withSelectionListEnabled(false)
      .withSavedInstance(savedInstanceState)
      .build();

    @SuppressWarnings("unused")
    Drawer result = new DrawerBuilder()
      .withActivity(this)
      .withToolbar(toolbar)
      .withHasStableIds(true)
      .withItemAnimator(new AlphaCrossFadeAnimator())
      .withAccountHeader(headerResult)
      .addDrawerItems(
        mUserProfile.getUserDrawerOptions()
      )
      .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
        @Override
        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
          return drawerItem != null && mUserProfile.handleOptionClick(view.getContext(), drawerItem.getIdentifier());
        }
      })
      .withSavedInstance(savedInstanceState)
      .build();

    //if you have many different types of DrawerItems you can magically pre-cache those items to get a better scroll performance
    //make sure to init the cache after the DrawerBuilder was created as this will first clear the cache to make sure no old elements are in
    //RecyclerViewCacheUtil.getInstance().withCacheSize(2).init(result);
    //new RecyclerViewCacheUtil<IDrawerItem>().withCacheSize(2).apply(result.getRecyclerView(), result.getDrawerItems());
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    //overridePendingTransition(R.animator.slide_right_in, R.animator.slide_right_out);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @AfterViews
  public void doAfterViews(){
    StorageWrapper.init();
  }

  private void setupViewPager(ViewPager viewPager) {
    ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

    adapter.addFragment(new EVentsFragment_(), getString(R.string.label_relax));
    adapter.addFragment(new EVentsFragment_(), getString(R.string.lable_learn));
    adapter.addFragment(new EatFragment_(), getString(R.string.label_eat));
    adapter.addFragment(new NewsFeedFragment_(), getString(R.string.label_news));
    //adapter.addFragment(new NewsFeedFragment_(), "CALENDAR");
    viewPager.setAdapter(adapter);
  }

  @Override
  protected void onStart() {
    super.onStart();
    mAuthListener = new FirebaseAuth.AuthStateListener() {
      @Override
      public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if ( user == null ){
          Intent intent = new Intent(getApplicationContext(), LoginActivity_.class);
          startActivity(intent);
          finish();
        }
    }};
    authHandler.register(mAuthListener);
  }

  @Override
  protected void onStop() {
    authHandler.unregister(mAuthListener);
    super.onStop();
  }

  class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    ViewPagerAdapter(FragmentManager manager) {
      super(manager);
    }

    @Override
    public Fragment getItem(int position) {
      return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
      return mFragmentList.size();
    }

    void addFragment(Fragment fragment, String title) {
      mFragmentList.add(fragment);
      mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return mFragmentTitleList.get(position);
    }
  }
}
